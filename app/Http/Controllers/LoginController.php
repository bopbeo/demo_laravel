<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Auth;

class LoginController extends Controller
{
    public function getLogin()
    {
    	return view('login');
    }

    public function postLogin(LoginRequest $request) 
    {
    	$email = $request->input('email');
    	$password = $request->input('password');

    	if(Auth::attempt(['email' => $email, 'password' => $password]))
    	{
    		return redirect()->intended('products');
    	} 
    	else 
    	{
    		return redirect()->back()->with('message', 'Tài khoản hoặc mật khẩu của bạn không đúng');
    	}
    }
}
