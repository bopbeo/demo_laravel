<!DOCTYPE html>
<html>
<head>
	<title>CRUD Application</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style type="text/css">
		.container{
			margin-top: 25px;
		}
		.location{
			margin-left: 1200px;
		}
	</style>
</head>
<body>
	<div class="d-flex flex-row-reverse bg-secondary">
    <div class="p-2 bg-info">Flex item 1</div>
  </div>
	<div class="dropdown">
		<button type="button" class="location btn dropdown-toggle" data-toggle="dropdown">
			{{Auth::user()->name}}
		<div class="dropdown-menu">
			<a class="dropdown-item" href="/logout">logout</a>
		</div>
	</div>
	<div class="container">
		@yield('content')
	</div>
</body>
</html>